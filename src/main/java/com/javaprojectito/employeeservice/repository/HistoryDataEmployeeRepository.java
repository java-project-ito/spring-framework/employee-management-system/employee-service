package com.javaprojectito.employeeservice.repository;

import com.javaprojectito.employeeservice.entity.HistoryDataEmployeeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface HistoryDataEmployeeRepository extends JpaRepository<HistoryDataEmployeeEntity, Long> {
    @Query(value = "SELECT COUNT(DISTINCT (employeeCode)) FROM HistoryDataEmployeeEntity")
    Long countEmp();

    @Query(value = "SELECT distinct employeeCode,activity, max(updatedAt) as updateAt FROM HistoryDataEmployeeEntity where deletedAt=null group by employeeCode")
    List<Object[]> listUndeletedHistory();

}
