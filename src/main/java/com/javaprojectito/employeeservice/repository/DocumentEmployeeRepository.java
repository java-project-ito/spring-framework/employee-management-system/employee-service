package com.javaprojectito.employeeservice.repository;
import com.javaprojectito.employeeservice.entity.DocumentEmployeeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface DocumentEmployeeRepository extends JpaRepository<DocumentEmployeeEntity,Long> {
    @Query(value = "SELECT fileData, typeDoc, employeeFileName FROM DocumentEmployeeEntity")
    List<Object[]> listUndeletedDocument();
}
