package com.javaprojectito.employeeservice.repository;

import com.javaprojectito.employeeservice.entity.DataEmployeeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface DataEmployeeRepository extends JpaRepository<DataEmployeeEntity,String> {
    @Query(value = "SELECT employeeName, employeeBirthPlace, employeeBirthDate,employeeAddress, employeeCodeDept, employeeJoinWork, employeeWorkStatusActivated, employeeLongWork, employeeCodeLevelPosition FROM DataEmployeeEntity")
    List<Object[]> listUndeletedData();
}
