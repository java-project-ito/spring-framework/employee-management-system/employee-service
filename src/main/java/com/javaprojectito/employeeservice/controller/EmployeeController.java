package com.javaprojectito.employeeservice.controller;

import com.javaprojectito.employeeservice.dto.RequestDto;
import com.javaprojectito.employeeservice.dto.ResponseDto;
import com.javaprojectito.employeeservice.dto.ResponseUndeletedDto;
import com.javaprojectito.employeeservice.dto.ResponseUndeletedHistoryDto;
import com.javaprojectito.employeeservice.entity.DataEmployeeEntity;
import com.javaprojectito.employeeservice.entity.DocumentEmployeeEntity;
import com.javaprojectito.employeeservice.entity.HistoryDataEmployeeEntity;
import com.javaprojectito.employeeservice.repository.HistoryDataEmployeeRepository;
import com.javaprojectito.employeeservice.service.EmployeeService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("api/employee")
public class EmployeeController {
    private EmployeeService employeeService;
    private HistoryDataEmployeeRepository historyDataEmployeeRepository;

    @PostMapping(value = "create")
    public ResponseEntity<ResponseDto> createService(@RequestParam("file") MultipartFile multipartFile,
                                                     @ModelAttribute RequestDto requestDto){
        return new ResponseEntity<>(employeeService.createdEmployee(multipartFile,requestDto), HttpStatus.CREATED);
    }

    @GetMapping(value = "view-data")
    public ResponseEntity<List<DataEmployeeEntity>> getDataEmployee(){
        return new ResponseEntity<>(employeeService.getDataEmployee(), HttpStatus.OK);
    }

    @GetMapping(value = "view-document")
    public ResponseEntity<List<DocumentEmployeeEntity>> getDocumentEmployee(){
        return new ResponseEntity<>(employeeService.getDocumentEmployee(), HttpStatus.OK);
    }

    @GetMapping(value = "view-history-data")
    public ResponseEntity<List<HistoryDataEmployeeEntity>> getHistoryDataEmployee(){
        return new ResponseEntity<>(employeeService.getHistoryDataEmployee(), HttpStatus.OK);
    }

    @GetMapping(value = "view-history-data-undeleted")
    public ResponseEntity<List<ResponseUndeletedHistoryDto>> getHistoryDataEmployeeUndeleted(){
        return new ResponseEntity<>(employeeService.getListUndeletedHistory(), HttpStatus.OK);
    }

    @GetMapping(value = "view-all")
    public ResponseEntity<List<ResponseUndeletedDto>> getEmployeeUndeleted(){
        return new ResponseEntity<>(employeeService.getListUndeleted(), HttpStatus.OK);
    }


}
