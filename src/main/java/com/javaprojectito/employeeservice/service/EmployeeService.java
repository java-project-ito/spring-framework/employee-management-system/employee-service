package com.javaprojectito.employeeservice.service;

import com.javaprojectito.employeeservice.dto.RequestDto;
import com.javaprojectito.employeeservice.dto.ResponseDto;
import com.javaprojectito.employeeservice.dto.ResponseUndeletedDto;
import com.javaprojectito.employeeservice.dto.ResponseUndeletedHistoryDto;
import com.javaprojectito.employeeservice.entity.DataEmployeeEntity;
import com.javaprojectito.employeeservice.entity.DocumentEmployeeEntity;
import com.javaprojectito.employeeservice.entity.HistoryDataEmployeeEntity;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface EmployeeService {

    ResponseDto createdEmployee(MultipartFile multipartFile, RequestDto requestDto);

    List<DataEmployeeEntity> getDataEmployee();

    List<DocumentEmployeeEntity> getDocumentEmployee();

    List<HistoryDataEmployeeEntity> getHistoryDataEmployee();

    List<ResponseUndeletedHistoryDto> getListUndeletedHistory();
    List<ResponseUndeletedDto> getListUndeleted();


}
