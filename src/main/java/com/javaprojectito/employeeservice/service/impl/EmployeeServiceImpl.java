package com.javaprojectito.employeeservice.service.impl;

import com.javaprojectito.employeeservice.dto.*;
import com.javaprojectito.employeeservice.entity.DataEmployeeEntity;
import com.javaprojectito.employeeservice.entity.DocumentEmployeeEntity;
import com.javaprojectito.employeeservice.entity.HistoryDataEmployeeEntity;
import com.javaprojectito.employeeservice.entity.generatorName.CapitalizeWords;
import com.javaprojectito.employeeservice.entity.genratorCodeEmployee.GeneratorCodeEmployee;
import com.javaprojectito.employeeservice.repository.DataEmployeeRepository;
import com.javaprojectito.employeeservice.repository.DocumentEmployeeRepository;
import com.javaprojectito.employeeservice.repository.HistoryDataEmployeeRepository;
import com.javaprojectito.employeeservice.service.EmployeeService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Service
public class EmployeeServiceImpl implements EmployeeService {
    private DataEmployeeRepository dataEmployeeRepository;
    private DocumentEmployeeRepository documentEmployeeRepository;
    private HistoryDataEmployeeRepository historyDataEmployeeRepository;
    @Override
    public ResponseDto createdEmployee(MultipartFile multipartFile, RequestDto requestDto) {
        DataEmployeeEntity dataEmployeeEntity=new DataEmployeeEntity();
        DocumentEmployeeEntity documentEmployeeEntity = new DocumentEmployeeEntity();
        HistoryDataEmployeeEntity historyDataEmployeeEntity = new HistoryDataEmployeeEntity();

        GeneratorCodeEmployee generatorCodeEmployee = new GeneratorCodeEmployee();
        Timestamp timestamp=new Timestamp(System.currentTimeMillis());
        CapitalizeWords capitalizeWords = new CapitalizeWords();
        dataEmployeeEntity.setId(generatorCodeEmployee.generatedCode(historyDataEmployeeRepository.countEmp(),requestDto));
        dataEmployeeEntity.setEmployeeName(capitalizeWords.capitalizeWords(requestDto.getEmployeeName()));
        dataEmployeeEntity.setEmployeeBirthPlace(capitalizeWords.capitalizeWords(requestDto.getEmployeeBirthPlace()));
        dataEmployeeEntity.setEmployeeBirthDate(requestDto.getEmployeeBirthDate());
        dataEmployeeEntity.setEmployeeAddress(requestDto.getEmployeeAddress());
        dataEmployeeEntity.setEmployeeCodeDept(requestDto.getEmployeeCodeDept());
        dataEmployeeEntity.setEmployeeJoinWork(requestDto.getEmployeeJoinWork());
        dataEmployeeEntity.setEmployeeWorkStatusActivated(requestDto.getEmployeeWorkStatusActivated());
        dataEmployeeEntity.setEmployeeLongWork(requestDto.getEmployeeLongWork());
        dataEmployeeEntity.setEmployeeCodeLevelPosition(requestDto.getEmployeeCodeLevelPosition());

        dataEmployeeRepository.save(dataEmployeeEntity);

        try {
            documentEmployeeEntity.setEmployeeCode(dataEmployeeEntity.getId());
            documentEmployeeEntity.setEmployeeFileName(multipartFile.getOriginalFilename());
            documentEmployeeEntity.setFileData(multipartFile.getBytes());
            documentEmployeeEntity.setTypeDoc(multipartFile.getContentType());

            documentEmployeeRepository.save(documentEmployeeEntity);
        }catch (IOException e){
            throw new RuntimeException(e);
        }

        historyDataEmployeeEntity.setEmployeeCode(dataEmployeeEntity.getId());
        historyDataEmployeeEntity.setActivity("Add Data");
        //historyDataEmployee.setCreatedAt(LocalDateTime.now().toLocalDate());
        historyDataEmployeeEntity.setCreatedAt(timestamp.toLocalDateTime());
        historyDataEmployeeEntity.setUpdatedAt(historyDataEmployeeEntity.getCreatedAt());

        historyDataEmployeeRepository.save(historyDataEmployeeEntity);

        ResponseDto responseDto = new ResponseDto();

        responseDto.setEmployeeCode(dataEmployeeEntity.getId());
        responseDto.setEmployeeName(dataEmployeeEntity.getEmployeeName());
        responseDto.setEmployeeBirthPlace(dataEmployeeEntity.getEmployeeBirthPlace());
        responseDto.setEmployeeBirthDate(dataEmployeeEntity.getEmployeeBirthDate());
        responseDto.setEmployeeAddress(dataEmployeeEntity.getEmployeeAddress());
        responseDto.setEmployeeCodeDept(dataEmployeeEntity.getEmployeeCodeDept());
        responseDto.setEmployeeJoinWork(dataEmployeeEntity.getEmployeeJoinWork());
        responseDto.setEmployeeWorkStatusActivated(dataEmployeeEntity.getEmployeeWorkStatusActivated());
        responseDto.setEmployeeLongWork(dataEmployeeEntity.getEmployeeLongWork());
        responseDto.setEmployeeCodeLevelPosition(dataEmployeeEntity.getEmployeeCodeLevelPosition());

        responseDto.setActivity(historyDataEmployeeEntity.getActivity());
        responseDto.setCreatedAt(historyDataEmployeeEntity.getCreatedAt());
        responseDto.setUpdatedAt(historyDataEmployeeEntity.getCreatedAt());

        responseDto.setFileType(documentEmployeeEntity.getTypeDoc());
        responseDto.setFileName(documentEmployeeEntity.getEmployeeFileName());
        responseDto.setUploadStatus(true);
        responseDto.setMessage("Upload Successfully");

        return responseDto;


    }

    @Override
    public List<DataEmployeeEntity> getDataEmployee() {
        List<DataEmployeeEntity> dataEmployeeEntityList = new ArrayList<>();
        for (DataEmployeeEntity dataEmployeeEntity:dataEmployeeRepository.findAll()){
            DataEmployeeEntity dataEmployee = new DataEmployeeEntity(
                    dataEmployeeEntity.getId(),
                    dataEmployeeEntity.getEmployeeName(),
                    dataEmployeeEntity.getEmployeeBirthPlace(),
                    dataEmployeeEntity.getEmployeeBirthDate(),
                    dataEmployeeEntity.getEmployeeAddress(),
                    dataEmployeeEntity.getEmployeeCodeDept(),
                    dataEmployeeEntity.getEmployeeJoinWork(),
                    dataEmployeeEntity.getEmployeeWorkStatusActivated(),
                    dataEmployeeEntity.getEmployeeLongWork(),
                    dataEmployeeEntity.getEmployeeCodeLevelPosition()
            );
            dataEmployeeEntityList.add(dataEmployee);
        }
        return dataEmployeeEntityList;
    }

    @Override
    public List<DocumentEmployeeEntity> getDocumentEmployee() {
        List<DocumentEmployeeEntity> documentEmployeeEntityList = new ArrayList<>();
        for(DocumentEmployeeEntity documentEmployeeEntity : documentEmployeeRepository.findAll()){
            DocumentEmployeeEntity documentEmployee = new DocumentEmployeeEntity(
                    documentEmployeeEntity.getId(),
                    documentEmployeeEntity.getEmployeeFileName(),
                    documentEmployeeEntity.getEmployeeCode(),
                    documentEmployeeEntity.getTypeDoc(),
                    documentEmployeeEntity.getFileData()
            );
            documentEmployeeEntityList.add(documentEmployee);
        }
        return documentEmployeeEntityList;
    }

    @Override
    public List<HistoryDataEmployeeEntity> getHistoryDataEmployee() {
        List<HistoryDataEmployeeEntity> historyDataEmployeesListEntity = new ArrayList<>();
        for(HistoryDataEmployeeEntity historyDataEmployeeEntity :historyDataEmployeeRepository.findAll()){
            HistoryDataEmployeeEntity historyData = new HistoryDataEmployeeEntity(
                    historyDataEmployeeEntity.getId(),
                    historyDataEmployeeEntity.getEmployeeCode(),
                    historyDataEmployeeEntity.getActivity(),
                    historyDataEmployeeEntity.getCreatedAt(),
                    historyDataEmployeeEntity.getUpdatedAt(),
                    historyDataEmployeeEntity.getDeletedAt()
            );
            historyDataEmployeesListEntity.add(historyData);
        }

        return historyDataEmployeesListEntity;
    }

    @Override
    public List<ResponseUndeletedHistoryDto> getListUndeletedHistory() {
        List<ResponseUndeletedHistoryDto> responseUndeletedHistoryDtoList = new ArrayList<>();

        for (int i = 0;i<historyDataEmployeeRepository.listUndeletedHistory().size();i++){

            ResponseUndeletedHistoryDto responseUndeletedHistoryDto = new ResponseUndeletedHistoryDto();
            responseUndeletedHistoryDto.setCodeEmployee((String) historyDataEmployeeRepository.listUndeletedHistory().get(i)[0]);
            responseUndeletedHistoryDto.setActivity((String) historyDataEmployeeRepository.listUndeletedHistory().get(i)[1]);
            responseUndeletedHistoryDto.setUpdatedAt((LocalDateTime) historyDataEmployeeRepository.listUndeletedHistory().get(i)[2]);
            responseUndeletedHistoryDtoList.add(responseUndeletedHistoryDto);
        }
        return responseUndeletedHistoryDtoList;
    }

    @Override
    public List<ResponseUndeletedDto> getListUndeleted() {
        List<ResponseUndeletedDto> responseUndeletedDtoList = new ArrayList<>();
        for(int i=0;i<getListUndeletedHistory().size();i++){
            ResponseUndeletedDto responseUndeletedDto = new ResponseUndeletedDto();
            responseUndeletedDto.setCodeEmployee(getListUndeletedHistory().get(i).getCodeEmployee());
            responseUndeletedDto.setEmployeeName(listUndeletedData().get(i).getEmployeeName());
            responseUndeletedDto.setEmployeeBirthPlace(listUndeletedData().get(i).getEmployeeBirthPlace());
            responseUndeletedDto.setEmployeeBirthDate(listUndeletedData().get(i).getEmployeeBirthDate());
            responseUndeletedDto.setEmployeeAddress(listUndeletedData().get(i).getEmployeeAddress());
            responseUndeletedDto.setEmployeeCodeDept(listUndeletedData().get(i).getEmployeeCodeDept());
            responseUndeletedDto.setEmployeeJoinWork(listUndeletedData().get(i).getEmployeeJoinWork());
            responseUndeletedDto.setEmployeeWorkStatusActivated(listUndeletedData().get(i).getEmployeeWorkStatusActivated());
            responseUndeletedDto.setEmployeeLongWork(listUndeletedData().get(i).getEmployeeLongWork());
            responseUndeletedDto.setEmployeeCodeLevelPosition(listUndeletedData().get(i).getEmployeeCodeLevelPosition());
            responseUndeletedDto.setNameFileEmployee(getListUndeletedDocument().get(i).getNameFileEmployee());
            responseUndeletedDto.setFileData(getListUndeletedDocument().get(i).getFileData());
            responseUndeletedDto.setTypeDoc(getListUndeletedDocument().get(i).getTypeDoc());
            responseUndeletedDto.setActivity(getListUndeletedHistory().get(i).getActivity());
            responseUndeletedDto.setUpdatedAt(getListUndeletedHistory().get(i).getUpdatedAt());

            responseUndeletedDtoList.add(responseUndeletedDto);

        }
        return responseUndeletedDtoList;
    }

    public List<ResponseUndeletedDocumentDto> getListUndeletedDocument (){
        List<ResponseUndeletedDocumentDto> responseUndeletedDocumentDtoList = new ArrayList<>();
        for(int i = 0 ; i < documentEmployeeRepository.listUndeletedDocument().size(); i++){
            ResponseUndeletedDocumentDto responseUndeletedDocumentDto = new ResponseUndeletedDocumentDto();
            responseUndeletedDocumentDto.setFileData((byte[]) documentEmployeeRepository.listUndeletedDocument().get(i)[0]);
            responseUndeletedDocumentDto.setTypeDoc((String) documentEmployeeRepository.listUndeletedDocument().get(i)[1]);
            responseUndeletedDocumentDto.setNameFileEmployee((String) documentEmployeeRepository.listUndeletedDocument().get(i)[2]);
            responseUndeletedDocumentDtoList.add(responseUndeletedDocumentDto);
        }
        return responseUndeletedDocumentDtoList;
    }

    public List<ResponseUndeletedDataDto> listUndeletedData(){
        List<ResponseUndeletedDataDto> responseUndeletedDataDtoList = new ArrayList<>();
        for(int i=0;i<dataEmployeeRepository.listUndeletedData().size();i++){
            ResponseUndeletedDataDto responseUndeletedDataDto = new ResponseUndeletedDataDto();
            responseUndeletedDataDto.setEmployeeName((String) dataEmployeeRepository.listUndeletedData().get(i)[0]);
            responseUndeletedDataDto.setEmployeeBirthPlace((String) dataEmployeeRepository.listUndeletedData().get(i)[1]);
            responseUndeletedDataDto.setEmployeeBirthDate((LocalDate) dataEmployeeRepository.listUndeletedData().get(i)[2]);
            responseUndeletedDataDto.setEmployeeAddress((String) dataEmployeeRepository.listUndeletedData().get(i)[3]);
            responseUndeletedDataDto.setEmployeeCodeDept((String) dataEmployeeRepository.listUndeletedData().get(i)[4]);
            responseUndeletedDataDto.setEmployeeJoinWork((LocalDate) dataEmployeeRepository.listUndeletedData().get(i)[5]);
            responseUndeletedDataDto.setEmployeeWorkStatusActivated((String) dataEmployeeRepository.listUndeletedData().get(i)[6]);
            responseUndeletedDataDto.setEmployeeLongWork((String) dataEmployeeRepository.listUndeletedData().get(i)[7]);
            responseUndeletedDataDto.setEmployeeCodeLevelPosition((String) dataEmployeeRepository.listUndeletedData().get(i)[8]);
            responseUndeletedDataDtoList.add(responseUndeletedDataDto);
        }
        return responseUndeletedDataDtoList;
    }

}
