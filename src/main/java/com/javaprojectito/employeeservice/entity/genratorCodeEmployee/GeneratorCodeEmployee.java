package com.javaprojectito.employeeservice.entity.genratorCodeEmployee;

import com.javaprojectito.employeeservice.dto.RequestDto;
import lombok.AllArgsConstructor;

import java.util.Random;

@AllArgsConstructor
public class GeneratorCodeEmployee {
    public String generatedCode(Long numEmp, RequestDto requestDto){
        String patterCompany = "MCLA1920";
        String patternEmployeeJoin = "0000000";
        Random random= new Random();
        String patternSubsString = patternEmployeeJoin.substring(0, (int) (patternEmployeeJoin.length()-numEmp.toString().length()));
        String[] arrDate=requestDto.getEmployeeJoinWork().toString().split("-");
        System.out.println(numEmp);
        return patterCompany+arrDate[0]+patternSubsString+numEmp+random.nextInt(0,9);
    }
}
