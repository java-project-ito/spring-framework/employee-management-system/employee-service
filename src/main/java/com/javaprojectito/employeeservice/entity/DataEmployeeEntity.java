package com.javaprojectito.employeeservice.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "data_employee")
public class DataEmployeeEntity {
    @Id
    @Column(name = "code_employee", nullable = false, unique = true, length = 20)
    private String id;
    @Column(name = "name_employee", nullable = false)
    private String employeeName;
    @Column(name = "birth_place")
    private String employeeBirthPlace;
    @Column(name = "birth_date", nullable = false)
    private LocalDate employeeBirthDate;
    @Column(name = "address", nullable = false)
    private String employeeAddress;
    @Column(name = "code_dept", nullable = false)
    private String employeeCodeDept;
    @Column(name = "date_join")
    private LocalDate employeeJoinWork;
    @Column(name = "status_activate")
    private String employeeWorkStatusActivated;
    @Column(name = "long_work")
    private String employeeLongWork;
    @Column(name = "code_position")
    private String employeeCodeLevelPosition;
}
