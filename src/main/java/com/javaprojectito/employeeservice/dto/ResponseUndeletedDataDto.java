package com.javaprojectito.employeeservice.dto;

import jakarta.persistence.Column;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ResponseUndeletedDataDto {
    private String employeeName;
    private String employeeBirthPlace;
    private LocalDate employeeBirthDate;
    private String employeeAddress;
    private String employeeCodeDept;
    private LocalDate employeeJoinWork;
    private String employeeWorkStatusActivated;
    private String employeeLongWork;
    private String employeeCodeLevelPosition;
}
