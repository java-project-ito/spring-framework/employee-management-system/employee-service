package com.javaprojectito.employeeservice.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ResponseUndeletedDto {
    private String codeEmployee;
    private String employeeName;
    private String employeeBirthPlace;
    private LocalDate employeeBirthDate;
    private String employeeAddress;
    private String employeeCodeDept;
    private LocalDate employeeJoinWork;
    private String employeeWorkStatusActivated;
    private String employeeLongWork;
    private String employeeCodeLevelPosition;
    private String activity;
    private String nameFileEmployee;
    private byte[] fileData;
    private String typeDoc;
    private LocalDateTime updatedAt;

}
