package com.javaprojectito.employeeservice.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.text.DateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class RequestDto {
    private String employeeName;
    private String employeeBirthPlace;
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private LocalDate employeeBirthDate;
    private String employeeAddress;
    private String employeeCodeDept;
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private LocalDate employeeJoinWork;
    private String employeeWorkStatusActivated;
    private String employeeLongWork;
    private String employeeCodeLevelPosition;
}
