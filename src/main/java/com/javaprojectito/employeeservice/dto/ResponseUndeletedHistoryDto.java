package com.javaprojectito.employeeservice.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;



@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ResponseUndeletedHistoryDto {
    private String codeEmployee;
    private String activity;
    private LocalDateTime updatedAt;
}
