package com.javaprojectito.employeeservice.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ResponseUndeletedDocumentDto {
    private String nameFileEmployee;
    private String typeDoc;
    private byte[] fileData;
}
